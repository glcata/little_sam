// * @author Catalin Glavan

import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class littleSam extends JFrame {

    private final JFrame frame;
    private final JPanel center;
    private final JLabel SAM;
    private final JPanel N;
    private final JPanel S;
    private final JPanel s_left;
    private final JPanel s_right;
    private final JPanel E;
    private final JPanel W;
    private final JButton makeHappy;
    private final JButton makeLove;
    private final JButton confused1;
    private final JButton confused2;
    private final JButton makeAngry;
    private final JButton mystery;
    private final Font setF;
    private final Color set_C = Color.ORANGE;
    private final String defPath = "/images/funny_Sam/faces/";
    private final String[] random_faces = {"wonderful.png", "inspired.png", "inspired2.png", "scared.png"};
    private final String[] faces = {"angry.png", "confused.png", "cry.png", "happy.png", "inspired.png", "inspired2.png", "love.png", "scared.png", "silly.png", "vampire.png", "very_angry.png", "wonderful.png", "wonderful2.png", "wonderful3.png"};
    private final String[] msg = {"GO AWAY !    ", "HRGGRAAA, I KILL YOU !", "I LIKE THIS, THANK YOU !", "WHAT IS YOUR NAME ?"};
    private final Color[] color = {Color.RED, Color.GREEN, Color.ORANGE, Color.BLUE, Color.MAGENTA, Color.BLACK};
    private final String syntBG = "Synthetica.background";
    private final String syntBG_a = "Synthetica.background.alpha";
    private static ImageIcon image = null;
    private Thread pics = null;
    private int maker = 10;
    private static int count = 0;

    public littleSam() {

        frame = new JFrame();
        frame.setLayout(new BorderLayout());
        frame.setBackground(Color.DARK_GRAY);
        setF = new Font("Calibri", Font.BOLD, 13);

        center = new JPanel();
        center.setBackground(Color.ORANGE);

        SAM = new JLabel(image);
        randomPICS(true);
        center.add(SAM);
        frame.add(center, BorderLayout.CENTER);

        N = new JPanel();
        N.setBackground(set_C);
        N.setPreferredSize(new Dimension(320, 40));
        makeHappy = new JButton("Make him HAPPY !");
        makeHappy.setFont(setF);
        makeHappy.putClientProperty(syntBG, color[1]);
        makeHappy.putClientProperty(syntBG_a, 0.20f);
        makeLove = new JButton("Make him LOVE !");
        makeLove.setFont(setF);
        makeLove.putClientProperty(syntBG, color[0]);
        makeLove.putClientProperty(syntBG_a, 0.20f);
        N.add(makeHappy);
        N.add(makeLove);

        S = new JPanel();
        S.setBackground(set_C);
        s_left = new JPanel();
        s_left.setPreferredSize(new Dimension(131, 60));
        s_left.setLayout(new BoxLayout(s_left, BoxLayout.LINE_AXIS));
        s_left.setBackground(set_C);
        makeAngry = new JButton(" Make him ANGRY");
        makeAngry.setFont(setF);
        makeAngry.putClientProperty(syntBG, color[0]);
        makeAngry.putClientProperty(syntBG_a, 0.70f);
        s_left.add(makeAngry);
        s_right = new JPanel();
        s_right.setPreferredSize(new Dimension(130, 60));
        s_right.setLayout(new BoxLayout(s_right, BoxLayout.LINE_AXIS));
        s_right.setBackground(set_C);
        mystery = new JButton("      Mystery ?!      ");
        mystery.setFont(setF);
        mystery.putClientProperty(syntBG, color[3]);
        mystery.putClientProperty(syntBG_a, 0.40f);
        s_right.add(mystery);
        S.add(s_left);
        S.add(s_right);

        W = new JPanel();
        W.setBackground(set_C);
        W.setLayout(new BoxLayout(W, BoxLayout.LINE_AXIS));
        W.setPreferredSize(new Dimension(55, 30));
        confused1 = new JButton("?");
        confused1.setFont(setF);
        W.add(confused1);

        E = new JPanel();
        E.setBackground(set_C);
        E.setLayout(new BoxLayout(E, BoxLayout.LINE_AXIS));
        E.setPreferredSize(new Dimension(38, 30));
        confused2 = new JButton("?");
        confused2.setFont(setF);
        E.add(confused2);

        frame.add(N, BorderLayout.NORTH);
        frame.add(S, BorderLayout.SOUTH);
        frame.add(E, BorderLayout.EAST);
        frame.add(W, BorderLayout.WEST);

        SAM.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Random z = new Random();
                if (maker == z.nextInt(maker + 1)) {
                    Thread angry = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JLabel stop = new JLabel("SAM : " + msg[0]);
                                stop.setFont(setF);
                                center.add(stop);
                                frame.invalidate();
                                frame.validate();
                                frame.repaint();
                                SAM.setIcon(new ImageIcon(getClass().getResource(defPath + faces[0])));
                                pics.suspend();
                                Thread.sleep(2000);
                                pics.resume();
                                center.remove(stop);
                                frame.invalidate();
                                frame.validate();
                                frame.repaint();
                            } catch (InterruptedException ex) {
                                Logger.getLogger(littleSam.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    });
                    angry.start();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        makeHappy.addActionListener(new EventHandler(defPath + faces[3], true, 2, makeHappy.hashCode()));
        makeLove.addActionListener(new EventHandler(defPath + faces[6], true, 3, makeLove.hashCode()));
        confused1.addActionListener(new EventHandler(defPath + faces[1], false, 4, 0));
        confused2.addActionListener(new EventHandler(defPath + faces[1], false, 4, 0));
        makeAngry.addActionListener(new EventHandler(defPath + faces[10], true, 1, makeAngry.hashCode()));
        mystery.addActionListener(new EventHandler(defPath + faces[9], false, 0, 0));

        confused1.addMouseMotionListener(new MouseMotionHandler(defPath + faces[5]));
        confused2.addMouseMotionListener(new MouseMotionHandler(defPath + faces[4]));
        W.addMouseMotionListener(new MouseMotionHandler(defPath + faces[5]));
        E.addMouseMotionListener(new MouseMotionHandler(defPath + faces[4]));
        s_left.addMouseMotionListener(new MouseMotionHandler(defPath + faces[12]));
        s_right.addMouseMotionListener(new MouseMotionHandler(defPath + faces[13]));
        makeAngry.addMouseMotionListener(new MouseMotionHandler(defPath + faces[12]));
        mystery.addMouseMotionListener(new MouseMotionHandler(defPath + faces[13]));

        frame.setTitle("LITTLE SAM");
        frame.setSize(315, 310);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setIconImage(new ImageIcon(getClass().getResource(defPath + "wonderful.png")).getImage());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private class MouseMotionHandler implements MouseMotionListener {

        private String path = null;
        private ImageIcon image = null;

        public MouseMotionHandler(String p) {
            this.path = p;
            image = new ImageIcon(getClass().getResource(path));
        }

        @Override
        public void mouseDragged(MouseEvent e) {

        }

        @Override
        public void mouseMoved(MouseEvent e) {
            SAM.setIcon(image);
        }

    }

    private class EventHandler implements ActionListener {

        private Random z = new Random();
        private String path = null;
        private ImageIcon image = null;
        private boolean random;
        private int set_MSG = 0;
        private ArrayList<JButton> set_Button = new ArrayList<>();
        private JLabel info = null;
        private JButton specify = null;
        private int hash;

        public EventHandler(String p, boolean set, int m, int hash_b) {
            this.path = p;
            this.random = set;
            this.set_MSG = m;
            this.hash = hash_b;
            set_Button.add((makeHappy.hashCode() == hash) ? makeHappy : null);
            set_Button.add((makeAngry.hashCode() == hash) ? makeAngry : null);
            set_Button.add((makeLove.hashCode() == hash) ? makeLove : null);
            image = new ImageIcon(getClass().getResource(path));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            class sam_set {

                private boolean check;
                private JButton specify;

                public sam_set(boolean ck, int c, JButton spec) {
                    this.check = ck;
                    this.specify = spec;
                    try {
                        if (check == true) {
                            info = new JLabel("SAM : " + msg[set_MSG]);
                        } else {
                            info = new JLabel("TRY AGAIN [" + ++count + "]    ");
                        }
                        info.setFont(setF);
                        center.add(info);
                        refreshFrame();
                        if (check == true) {
                            SAM.setIcon(image);
                            pics.suspend();
                        }
                        int time = (check == true) ? 2000 : 60;
                        Thread.sleep(time);
                        if (check == true) {
                            pics.resume();
                        }
                        center.remove(info);
                        refreshFrame();
                        if (check == true) {
                            specify.setEnabled(true);
                        }
                    } catch (InterruptedException ex) {
                        Logger.getLogger(littleSam.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if (random) {
                Thread sam = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        boolean check;
                        if (check = (maker == z.nextInt(maker + 1))) {
                            count = 0;
                            for (int i = 0; i < set_Button.size(); i++) {
                                if (set_Button.get(i) != null) {
                                    set_Button.get(i).setEnabled(false);
                                    specify = set_Button.get(i);
                                }
                            }
                            new sam_set(check, count, specify);
                        } else {
                            new sam_set(check, count, specify);
                        }
                    }
                });
                sam.start();
            } else {
                SAM.setIcon(image);
            }
        }
    }

    private void refreshFrame() {
        frame.invalidate();
        frame.validate();
        frame.repaint();
    }

    private void randomPICS(boolean set) {
        Random z = new Random();
        if (set) {
            pics = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            SAM.setIcon(new ImageIcon(getClass().getResource(defPath + random_faces[z.nextInt(4)])));
                            Thread.sleep(800);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(littleSam.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
            pics.start();
        }
    }

    public static void main(String[] args) {
        try {
            javax.swing.UIManager.setLookAndFeel(new SyntheticaPlainLookAndFeel());
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new littleSam();
                }
            });
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(littleSam.class.getName()).log(java.util.logging.Level.WARNING, "ALERT", e);
        }
    }
}
